<?php

namespace Controllers;
use Models\Repository\CategorieRepository;

class HomeController{

    public function index() {
        require_once 'Views/view-home.php';
    }
}
