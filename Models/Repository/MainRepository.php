<?php
namespace Models\Repository;

abstract class MainRepository{

    public function find($table, $id){
        $sql = "SELECT * FROM " . $table . " WHERE id = :id";
        return ConnectDatabase::getInstance()->fetch($sql, [':id' => $id]);
    }

    public function findAll($table){
        $sql = "SELECT * FROM " . $table;
        return ConnectDatabase::getInstance()->fetchAll($sql);
    }

}
