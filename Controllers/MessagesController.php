<?php

namespace Controllers;
use Models\Repository\MessagesRepository;
use Models\Repository\UserRepository;

class MessagesController{
    public function index(){
      $messagesReposity = new MessagesRepository();
      $messages = $messagesReposity->getMessageByUtilisateur($_SESSION['utilisateur']->getId());
      require_once 'Views/view-mes-messages.php';
    }

    public function showMessagesRecu(){
        $messagesRepository = new MessagesRepository();
        $messages = $messagesRepository->getMessageRecu($_SESSION['utilisateur']->getId());
        require_once 'Views/view-messages-recu.php';
    }

    public function create(){
        $userRepositoy = new UserRepository();
        $users = $userRepositoy->findAll('utilisateurs');

        require_once 'Views/view-create-message.php';

        if(!empty($_POST['submitSendMessage'])){
            if(!empty($_POST['contentMessage']) && !empty($_POST['idUserTo'])){
                $messageRepository = new MessagesRepository();
                $messageRepository->setMessage($_SESSION['utilisateur']->getId(), $_POST['idUserTo'], $_POST['contentMessage']);
                echo "<script>window.location='?p=mes-messages'</script>";
            } else {
                $_SESSION['error'] = "Veuillez remplir tous les champs";
                echo "<script>window.location='?p=message'</script>";
            }
        }
    }
}
