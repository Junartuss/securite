<?php

namespace Models\Repository;


class MessagesRepository extends MainRepository{

    public function getMessageByUtilisateur($idUtilisateur) {
        $sql = "SELECT * FROM messages WHERE idExpediteur = :idExpediteur ORDER BY created_at DESC";
        return ConnectDatabase::getInstance()->fetchAll($sql, [':idExpediteur' => $idUtilisateur]);
    }

    public function getMessageRecu($idUtilisateur){
        $sql = "SELECT * FROM messages WHERE idDestinataire = :idDestinataire ORDER BY created_at DESC";
        return ConnectDatabase::getInstance()->fetchAll($sql, [':idDestinataire' => $idUtilisateur]);
    }

    public function setMessage($idExpediteur, $idDestinataire, $contenu){
        $sql = "INSERT INTO messages(contenu, idExpediteur, idDestinataire, created_at) VALUES(:contenu, :idExpediteur, :idDestinataire, NOW())";
        ConnectDatabase::getInstance()->execute($sql, [
            ':contenu' => htmlspecialchars($contenu),
            ':idExpediteur' => $idExpediteur,
            ':idDestinataire' => $idDestinataire,
        ]);
        return true;
    }
}
