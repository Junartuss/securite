<?php

namespace Models\Repository;
use Models\Entity\User;

class UserRepository extends MainRepository{
    public function insertDatabase(User $user){
        $passwordCrypt = password_hash($user->getMdp(), PASSWORD_DEFAULT);
        $sql = "INSERT INTO utilisateurs(nom, prenom, login, mdp) VALUES(:nom, :prenom, :login, :mdp)";
        ConnectDatabase::getInstance()->execute($sql, [
            ':nom' => $user->getNom(),
            ':prenom' => $user->getPrenom(),
            ':login' => $user->getLogin(),
            ':mdp' => $passwordCrypt,
        ]);
    }

    public function checkLoginUse(User $user){
        $users = $this->findAll("utilisateurs");
        $result = true;
        foreach($users as $unUser){
            if($unUser['login'] == $user->getLogin()){
                $result = false;
            }
        }
        return $result;
    }

    public function connectUser($login, $mdp){
        $array = [];

        $sql = "SELECT * FROM utilisateurs WHERE login = :login";
        $result = ConnectDatabase::getInstance()->fetch($sql, [':login' => $login]);

        if(password_verify($mdp, $result['mdp'])){
            $array['mdp'] = true;
            $array['utilisateur'] = new User($result['id'], $result['nom'], $result['prenom'], $result['login'], $mdp, $result['photo']);
        } else {
            $array['mdp'] = false;
        }

        return $array;
    }

    public static function getUserById($id){
        $sql = "SELECT * FROM utilisateurs WHERE id = :id";
        return ConnectDatabase::getInstance()->fetch($sql, [':id' => $id]);
    }

    public static function updatePhotoUser($idUser, $fileName){
        $sql = "UPDATE utilisateurs SET photo = :photo WHERE id = :id";
        return ConnectDatabase::getInstance()->execute($sql, [
            ':id' => $idUser,
            ':photo' => $fileName
        ]);
    }

    /*public function updateDatabase(User $user){
        $req = $this->connectDatabase()->prepare('UPDATE users SET nom = :nom, prenom = :prenom, mail = :mail WHERE id = :id');
        $req->execute(array(
            ':nom' => $user->getNom(),
            ':prenom' => $user->getPrenom(),
            ':mail' => $user->getMail(),
            ':id' => $user->getId()
        ));
        return $this;
    }

    public function updatePasswordDatabase(User $user){
        $passwordCrypt = password_hash($user->getPassword(), PASSWORD_DEFAULT);
        $req = $this->connectDatabase()->prepare('UPDATE users SET password = :password WHERE id = :id');
        $req->execute(array(
            ':password' => $passwordCrypt,
            ':id' => $user->getId()
        ));
        return $this;
    }

    public function deleteDatabase(User $user){
        $req = $this->connectDatabase()->prepare('DELETE FROM users WHERE id = :id');
        $req->execute(array(':id' => $user->getId()));
        return $this;
    }

    public function searchUser($id){
        $req = $this->connectDatabase()->prepare('SELECT * FROM users WHERE id = :id');
        $req->execute(array(':id' => $id));
        $result = $req->fetch();

        $user = new User($result['id'], $result['nom'], $result['prenom'], $result['mail'], $result['password'], $result['isAdmin']);
        return $user;
    }*/
}
