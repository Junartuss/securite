<?php

namespace Controllers;
use Models\Repository\ConnectDatabase;
use Models\Repository\UserRepository;

class LoginController{
    public function index(){
        require_once 'Views/view-login.php';

        if(isset($_POST['login'])) {
          if((!empty($_POST['login'])) && (!empty($_POST['password']))) {
            $userRepository = new UserRepository();
            $result = $userRepository->connectUser(htmlspecialchars($_POST['login']), htmlspecialchars($_POST['password']));
            if($result['mdp']){
                $_SESSION['utilisateur'] = $result['utilisateur'];
                echo "<script>window.location='?p=home'</script>";
            } else {
                $_SESSION['error'] = "Identifiants incorrects";
                echo "<script>window.location='?p=log-in'</script>";
            }

          }
        }
    }
}
