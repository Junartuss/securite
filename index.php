<?php

function my_autoloader($class) {
    $class = str_replace("\\", "/", $class);
    require_once __DIR__ . '/' . $class . '.php';
}
// register the autoloader
spl_autoload_register('my_autoloader');

// Appelle des controlleurs
use \Controllers\HomeController;
use \Controllers\LoginController;
use \Controllers\LogoutController;
use \Controllers\RegisterController;
use \Controllers\MessagesController;
use \Controllers\ProfilController;

// Instanciation
$homeController = new HomeController();
$loginController = new LoginController();
$logoutController = new LogoutController();
$registerController = new RegisterController();
$messagesController = new MessagesController();
$profilController = new ProfilController();


session_start();

if(!isset($_GET['p']) || $_GET['p'] == "home"){
  $homeController->index();
} else if($_GET['p'] == 'log-in') {
  $loginController->index();
} else if($_GET['p'] == 'register') {
  $registerController->index();
} else if($_GET['p'] == 'log-out') {
    if(isset($_SESSION['utilisateur'])){
        $logoutController->index();
    } else {
        header('Location: ?p=log-in');
    }
} else if($_GET['p'] == "mes-messages") {
    if(isset($_SESSION['utilisateur'])){
        $messagesController->index();
    } else {
        header('Location: ?p=log-in');
    }
} else if($_GET['p'] == "message"){
    if(isset($_SESSION['utilisateur'])){
        $messagesController->create();
    } else {
        header('Location: ?p=log-in');
    }
} else if($_GET['p'] == "messages-recu"){
    if(isset($_SESSION['utilisateur'])){
        $messagesController->showMessagesRecu();
    } else {
        header('Location: ?p=log-in');
    }
} else if($_GET['p'] == "profil"){
    if(isset($_SESSION['utilisateur'])){
        $profilController->index();
    } else {
        header('Location: ?p=log-in');
    }
}
