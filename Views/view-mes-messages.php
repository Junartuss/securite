<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Site</title>
  </head>
  <body>

      <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="?page=home">Sécurité</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                      <a class="nav-link" href="?page=home">Accueil</a>
                  </li>
                  <?php if(isset($_SESSION['utilisateur'])) { ?>
                      <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                              Messages
                          </a>
                          <div class="dropdown-menu">
                              <a class="dropdown-item" href="?p=message">Envoyer un message</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="?p=mes-messages">Messages envoyés</a>
                              <a class="dropdown-item" href="?p=messages-recu">Messages reçus</a>
                          </div>
                      </li>
                      <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                              Profil
                          </a>
                          <div class="dropdown-menu">
                              <a class="dropdown-item" href="?p=profil">Gestion du profil</a>
                              <a class="dropdown-item" href="?p=log-out">Se déconnecter</a>
                          </div>
                      </li>
                  <?php } else { ?>
                      <li class="nav-item">
                          <a class="nav-link" href="?p=log-in">Se connecter</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="?p=register">S'inscrire</a>
                      </li>
                  <?php } ?>
              </ul>
          </div>
      </nav>
    <br><br>

    <h2 class="text-center">Mes messages</h2>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Message</th>
                        <th scope="col">Destinataire</th>
                        <th scope="col">Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($messages as $message) { ?>
                        <tr>
                            <td style="width: 70%;"><?php echo $message['contenu']; ?></td>
                            <td><?php echo \Models\Repository\UserRepository::getUserById($message['idDestinataire'])['login']; ?></td>
                            <td><?php echo date("d-m-Y", strtotime($message['created_at'])); ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
