<?php

namespace Traits;

trait UploadImage{

    public function uploadImage($file, $taille_maxi = 1000000){

        $dossier = 'uploads/';
        $fichier = basename($file['name']);
        $taille = filesize($file['tmp_name']);
        $extensions = array('.png', '.jpg', '.jpeg');
        $extension = strrchr($file['name'], '.');

        if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
        {
            $_SESSION['error'] = 'Vous devez uploader un fichier de type png, jpg, ou jpeg';
            return false;
        }

        if( $taille > $taille_maxi)
        {
            $_SESSION['error'] = 'Le fichier est trop gros';
            return false;
        }

        $fichier = strtr($fichier,
            'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
            'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
        $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
        if(move_uploaded_file($file['tmp_name'], $dossier . $fichier))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}