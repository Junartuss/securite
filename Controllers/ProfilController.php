<?php

namespace Controllers;

use Models\Repository\UserRepository;
use Traits\UploadImage;

class ProfilController{
    use UploadImage;

    public function index(){

        require_once 'Views/view-profil.php';

        if(!empty($_POST['sendFormFile'])){
            if(empty($_FILES['inputFilePhoto']['name'])){
                $_SESSION['error'] = "Veuillez sélectionner une image";
                echo "<script>window.location='?p=profil'</script>";
            } else {
                if($this->uploadImage($_FILES['inputFilePhoto'])){
                    $_SESSION['utilisateur']->setPhoto($_FILES['inputFilePhoto']['name']);
                    UserRepository::updatePhotoUser($_SESSION['utilisateur']->getId(), $_SESSION['utilisateur']->getPhoto());
                    echo "<script>window.location='?p=profil'</script>";
                } else {
                    echo "<script>window.location='?p=profil'</script>";
                }
            }
        }

    }

}
