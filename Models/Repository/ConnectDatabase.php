<?php

namespace Models\Repository;

class ConnectDatabase{

    protected $_pdo = null;
    protected static $_instance;

    public function __construct()
    {
        if( $this->_pdo !== null ){
            return $this->_pdo;
        }

        $this->_pdo = new \PDO('mysql:host=localhost;dbname=securite;charset=utf8;', 'root', 'root');
        return $this->_pdo;
    }

    public static function getInstance()
    {
        if(!isset(self::$_instance))
            self::$_instance = new ConnectDatabase();
        return self::$_instance;
    }

    public function prepare($sql){
        return $this->_pdo->prepare($sql);
    }

    public function execute($sql, $bindValues = array()){
        $req = $this->_pdo->prepare($sql);
        $req->execute($bindValues);
        return $req;
    }

    public function fetch($sql, $bindValues=array()){
        $req = $this->_pdo->prepare($sql);
        $req->execute($bindValues);
        $res =  $req->fetch(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function fetchAll($sql, $bindValues=array()){
        $req = $this->execute($sql, $bindValues);
        $res = $req->fetchAll(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function query($sql){
        $this->_pdo->query($sql);
        return true;
    }

}