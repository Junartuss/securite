<?php

namespace Controllers;
use Models\Repository\UserRepository;
use Models\Entity\User;


class RegisterController{
    public function index(){
        require_once 'Views/view-register.php';

        if(isset($_POST['nom'])) {
          if((!empty($_POST['nom'])) && (!empty($_POST['prenom'])) && (!empty($_POST['login'])) && (!empty($_POST['password']))) {
              if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{8,}$#', $_POST['password'])) {
                  $user = new User(0, htmlspecialchars($_POST['nom']), htmlspecialchars($_POST['prenom']), htmlspecialchars($_POST['login']), htmlspecialchars($_POST['password']), htmlspecialchars($_POST['photo']));
                  $userReposity = new UserRepository();
                  if($userReposity->checkLoginUse($user) == true){
                      $userReposity->insertDatabase($user);
                      echo '<script>alert("inscription réussie!");</script>';
                      echo "<script>window.location='?p=log-in'</script>";
                  } else {
                      echo '<script>alert("login déjà utilisé!");</script>';
                      echo "<script>window.location='?p=register'</script>";
                  }
              } else {
                  echo '<script>alert("Le mot de passe n\'est pas conforme, il doit contenir 8 caractères minimum");</script>';
                  echo "<script>window.location='?p=register'</script>";
              }
          } else {
            echo '<script>alert("veuillez remplir tous les champs!");</script>';
            echo "<script>window.location='?p=register'</script>";
          }
        }
    }
}
