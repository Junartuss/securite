<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Site</title>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="?page=home">Sécurité</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                  <a class="nav-link" href="?page=home">Accueil</a>
              </li>
              <?php if(isset($_SESSION['utilisateur'])) { ?>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                          Messages
                      </a>
                      <div class="dropdown-menu">
                          <a class="dropdown-item" href="?p=message">Envoyer un message</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="?p=mes-messages">Messages envoyés</a>
                          <a class="dropdown-item" href="?p=messages-recu">Messages reçus</a>
                      </div>
                  </li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                          Profil
                      </a>
                      <div class="dropdown-menu">
                          <a class="dropdown-item" href="?p=profil">Gestion du profil</a>
                          <a class="dropdown-item" href="?p=log-out">Se déconnecter</a>
                      </div>
                  </li>
              <?php } else { ?>
                  <li class="nav-item">
                      <a class="nav-link" href="?p=log-in">Se connecter</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="?p=register">S'inscrire</a>
                  </li>
              <?php } ?>
          </ul>
      </div>
  </nav>

    <br><br>

    <h2 class="text-center">S'enregistrer</h2>

    <br>

    <div class="container">
        <?php if(!empty($_SESSION['error'])){ ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $_SESSION['error']; ?>
            </div>
            <?php unset($_SESSION['error']); ?>
        <?php } ?>
      <form action="?p=register" method="post">
        <div class="form-group">
          <label for="inputNom">Nom :</label>
          <input type="text" class="form-control" id="inputNom" name="nom">
        </div>

        <div class="form-group">
          <label for="inputPrenom">Prénom :</label>
          <input type="text" class="form-control" id="inputPrenom" name="prenom">
        </div>

        <div class="form-group">
          <label for="inputLogin">Login :</label>
          <input type="text" class="form-control" id="inputLogin" name="login">
        </div>

        <div class="form-group">
          <label for="inputPassword">Password :</label>
          <input type="password" class="form-control" id="inputPassword" name="password">
        </div>

        <input type="submit" class="btn btn-primary" value="Envoyer">
      </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
