<?php

namespace Models\Entity;

class User{
    private $id;
    private $nom;
    private $prenom;
    private $login;
    private $mdp;
    private $photo;

    public function __construct($id, $nom, $prenom, $login, $mdp, $photo)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->login = $login;
        $this->mdp = $mdp;
        $this->photo = $photo;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
        return $this;
    }

    public function getNom(){
        return $this->nom;
    }

    public function setNom($nom){
        $this->nom = $nom;
        return $this;
    }

    public function getPrenom(){
        return $this->prenom;
    }

    public function setPrenom($prenom){
        $this->prenom = $prenom;
        return $this;
    }

    public function getLogin() {
      return $this->login;
    }

    public function setLogin($login) {
      $this->login = $login;
    }

    public function getMdp() {
      return $this->mdp;
    }

    public function setMdp($mdp) {
      $this->mdp = $mdp;
    }

    public function getPhoto() {
      return $this->photo;
    }

    public function setPhoto($photo) {
      $this->photo = $photo;
    }
}
